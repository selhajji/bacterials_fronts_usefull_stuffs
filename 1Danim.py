import os
os.environ['PATH']=os.environ['PATH']+':/usr/local/bin'
import matplotlib
import matplotlib.pyplot as pl
import numpy as np
import matplotlib.animation as anim
import matplotlib.axes as ax
from numpy import fft as f


'''
Animation 1D standalone
'''


## load
frames=np.load('/../../testphi4_2_1f.npy')
T=np.load('/../../testphi4_2_1t.npy')
print(len(frames))


## filter / preprocess

sk=5 #skip frames (keep one frame other sk)
Q=int(len(T)/2) #troncate 

T=[T[k] for k in range(Q) if (k-1)%sk==0]
frames=[frames[k] for k in range(Q) if (k-1)%sk==0]
frames=np.array(frames)


## parametrize animation
matplotlib.use('Agg')
Writer = anim.writers['ffmpeg']
writer = Writer(fps=10, metadata=dict(artist='Me'), bitrate=1800)

L=1200
N=16384

lb=0.2515
kappa=1.1*10**-4
D0=1
alpha=0.1
u0=11
def D(u):
    return D0*np.exp(-lb*u)*(1-lb*u/2)
qc=np.sqrt(2*alpha/abs(D(u0)))


e=0.1
phi=1.8
x=np.linspace(-L/2,L/2,N)

fig = pl.figure(figsize=(11,5)); axes = fig.add_subplot(111)
line, = axes.plot([],[])

axes.set_xlabel('x (µm)')
axes.set_ylabel('u')
#axes.set_xlim((-np.sqrt(2)*L/2,np.sqrt(2)*L/2))
axes.set_xlim((0,L/6))
axes.set_ylim((0,18.2))
#xl=np.linspace(-l,l,len(frames[2]))
#fig.suptitle('bacterial scaled '+r'$\epsilon=$'+str(e)+' '+r'$\phi=$'+str(phi), fontsize=12)
#fig.suptitle('supercritical slow '+r'$\alpha=$'+str(alpha)+' '+r'$\lambda =$'+str(lb)+' '+r'$\kappa=$'+str(kappa)+' '+r'$u_0=$'+str(u0), fontsize=10)
#fig.suptitle('SH front '+r'$\epsilon=$'+str(e), fontsize=12)
#+' '+r'$\sigma q_c=$'+str(sigma*qc)[0:2]
def plot_frame(i):
    line.set_data(x,frames[i])#frames[i,:,257] #np.diagonal(frames[i])[:,513]#frames[i,:,513]
    t=str(T[i])
    if T[i]<0.1:
        axes.set_title('t='+t[0:7]+'s')
    elif T[i]<1:
        axes.set_title('t='+t[0:5]+'s')
    elif T[i]<10:
        axes.set_title('t='+t[0:3]+'s')
    else:
        k=int(np.log10(T[i]))+1
        axes.set_title('t='+t[0:k]+'s')
    fig.canvas.draw()
    return fig

# Animate the solution
animation=anim.FuncAnimation(fig, plot_frame, frames=len(T), interval=100, repeat=False)
animation.save("new_2_psinodal_zoom2.mp4", writer=writer)
