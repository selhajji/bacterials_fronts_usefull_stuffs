import numpy as np

''' 
Just a few functions and lines of codes to compute the emerging parameters of the bacterial equation
from the physical ones, to copy/past in more meaningful codes when needed
'''


## "physical" params and input to be set ##
lb=0.27
kappa=10**-3
D0=1
alpha=0.1
u0=11

phi=lb*u0/2
R=D0/np.sqrt(alpha*kappa) #R: usefull to compute it in the code, just to check that R>Rc (1)

def D(u): #the diffusion coefficient
    return D0*np.exp(-lb*u)*(1-lb*u/2)

def Rcr(phi): #Rc: usefull to compute it in the code, just to check that R>Rc (2)
    return 2*np.exp(2*phi)/(phi-1)
qc=np.sqrt(abs(D(u0))/(2*kappa))
lc=2*np.pi/qc
Rc=Rcr(phi)
e=(R-Rc)/Rc

leb=np.sqrt(D0/(6*kappa)*(1-np.sqrt(1-12*kappa*alpha/D0**2)))
vb=2*leb*(D0-2*kappa*leb**2)

leh=np.sqrt(np.abs(D(u0))/(24*kappa)*(np.sqrt(7-24*kappa/D(u0)**2*alpha)-1))
qeh=np.sqrt(3*leh**2-D(u0)/(2*kappa))
l=2*np.pi/qeh

vh=4*leh*(8*kappa*leh**2-D(u0))

## parameters of the upper front, but computed at any point (above spinodal) and not especially at rho_0 ##
def le(u):
    return np.sqrt(np.abs(D(u))/(24*kappa)*(np.sqrt(7+24*kappa/D(u)**2*alpha*(1-2*u/u0))-1))
def qe(u):
    return np.sqrt(3*le(u)**2-D(u)/(2*kappa))
def v(u):
    return 4*le(u)*(8*kappa*le(u)**2-D(u))
def tau(q):
    return -1/(alpha+D(u0)*q**2+kappa*q**4)

## parameters (coefficients and amplitude) of the amplitude equation developpement (cf original paper)
a=9/2*(1-phi)**2/(34*phi**4-56*phi**3-24*phi**2+31*phi+19)
#Aa=2*np.sqrt(-a*e)

def A(phi,R):
    e=(R-Rcr(phi))/Rcr(phi)
    A=e*18*(1-phi)**2/(34*phi**4-56*phi**3-24*phi**2+31*phi+19)
    return np.sqrt(-A)
def B(phi,e):
    A=e*18*(1-phi)**2/(34*phi**4-56*phi**3-24*phi**2+31*phi+19)
    return np.sqrt(-A)