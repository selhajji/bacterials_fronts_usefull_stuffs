import numpy as np
import numpy.fft as f #fast Fourier transform module

'''
This is a Cranck-Nicholson + Adam-Bashforth 3 code (CNAB3)

semi-implicit:
-CN (implicit method) for the linear part
-AB3 (explicit) for the non-linear part, a multistep method

Note that using a semi-implicit method is computionally useful as the most unstable term is linear
e.g the term in nabla^4

Semi-implicit simulations tends to be faster, as they need less operations per iteration
However they turned out to behaves very baddly at boundaries (explosion)
An irrelevant details when one is looking at only one phenomena
Here there are several time scaled involved: the simulation need to last after the FKPP front reached the borders
'''

T=[0]
t=0
L=600

N=4096

#
x=np.linspace(-L/2,L/2,N)
xi=f.rfftfreq(N)*N*2*np.pi/L
filtre = np.ones_like(xi)
filtre[np.where(np.abs(xi)>np.max(xi)*2./3)] = 0.

# auxiliaries functions nabla, nabla4 are not used anymore: 
# everything is done at hand to minimize the number of forward and backward FFT

lb=0.27
kappa=10**-3
D0=1
alpha=0.1
u0=11
phi=lb*u0/2
R=D0/np.sqrt(alpha*kappa)

def D(u):
    return D0*np.exp(-lb*u)*(1-lb*u/2)

#initial condition, this does not change
sigma=L/120
u=1/np.sqrt(2*np.pi*sigma**2)*np.exp(-x**2/(2*sigma**2))
u*=1/np.max(u)*1.1*u0
uh=f.rfft(u)
frames=[u.copy()]
Lh=alpha-D0*xi**2-kappa*xi**4 # linear part (in Fourier): just a linear operator

def Fh(u): # non-lineare part (in Fourier): a (non-linear) function
    return (1j*xi)*f.rfft((D(u)-D0)*f.irfft(1j*xi*uh))-alpha/u0*f.rfft(u**2)


skip=2000
Dt=10**-5

##initialization of the AB3 part
#choice made: init by two explicit Euler steps before implementing AB3 scheme
#step 1
fh2=Fh(u)
uh=(2*Dt*fh2/(2-Dt*Lh)+uh*(2+Dt*Lh)/(2-Dt*Lh))*filtre
t+=Dt
u=f.irfft(uh)
u=u*(u>10**-8)
#step 2
fh1=Fh(u)
uh=(2*Dt*fh1/(2-Dt*Lh)+uh*(2+Dt*Lh)/(2-Dt*Lh))*filtre
t+=Dt
u=f.irfft(uh)
u=u*(u>10**-8)

for k in range(10*10**6):
    fh0=Fh(u)
    F=23/12*fh0-4/3*fh1+5/12*fh2 #BA3: f_{n+1} = 23/12f_{n-2} - 4/3f_{n-1} + 5/12f_n
    uh=(2*Dt*F/(2-Dt*Lh)+uh*(2+Dt*Lh)/(2-Dt*Lh))*filtre
    t+=Dt
    u=f.irfft(uh)
    u=u*(u>10**-8)
    fh2=fh1 #f_{n-2}<- f_{n-1}
    fh1=fh0 # f_{n-1}>- f_n
    if int(k/skip)==k/skip:
        if np.isnan(np.sum(u)):
            print('nope')
            break
        print(np.min(u),np.max(u))
        frames.append(u.copy())
        T.append(t)