import os
os.environ['PATH']=os.environ['PATH']+':/usr/local/bin'
import matplotlib
import matplotlib.pyplot as pl
import numpy as np
import matplotlib.animation as anim
import matplotlib.axes as ax
from numpy import fft as f
from tqdm import tqdm

frames=np.load('.../.../_1f.npy') # density frames
T=np.load('.../.../_1t.npy') # correspindg times

## use the simulation parameters!! ##
lb=0.27
kappa=10**-3
D0=1
alpha=0.1
u0=11

M=len(frames)
m=0
skip=10
T=T[m:M:skip]
frames=frames[m:M:skip]
Lx,Ly=(80,80) #use those of the simulation
Nx,Ny=(256,256) #idem, could be automated from frames shape
x,y=np.mgrid[-Lx/2:Lx/2:1j*Nx,-Ly/2:Ly/2:1j*Ny]

## roughly nothing to tune here... except the title ##

matplotlib.use('Agg') #this set matlotlib in non graphic mode, if plots have already been made in the current kernel, it cannot be set and kernet has to be restarted
Writer = anim.writers['ffmpeg'] #using mp4 driver ffmpeg, has to be installed on computer, on linux or Mac OS it is straiforward with a package manager e.g "brew install ffmpeg"
writer = Writer(fps=3, metadata=dict(artist='Me'), bitrate=1800) #choose fps, mostly for control video duration and speed, not for the quality...

fig = pl.figure(figsize=(11,5)); axes = fig.add_subplot(111)
line= pl.imshow(frames[0], animated=True, extent=[-Lx,Lx,-Ly,Ly])
axes.set_xlabel('x (µm)')
axes.set_xlabel('y (µm)')

#the tiltle, it is a proposal, however it is very important to choose explicit titles of videos to avoid being lost later
fig.suptitle(r'$\alpha=$'+str(alpha)+' '+r'$\lambda =$'+str(lb)+' '+r'$\kappa=$'+str(kappa), fontsize=12)
cb=pl.colorbar()
cb.ax.set_ylabel('u')

def plot_frame(i):
    line.set_array(frames[i])
    t=str([i])
    if T[i]<0.1:
        axes.set_title('t='+t[0:7]+'s')
    elif T[i]<1:
        axes.set_title('t='+t[0:5]+'s')
    elif T[i]<10:
        axes.set_title('t='+t[0:3]+'s')
    else:
        k=int(np.log10(T[i]))+1
        axes.set_title('t='+t[0:k]+'s')
    fig.canvas.draw()
    return fig

# Animate the solution 

#main instruction:
animation=anim.FuncAnimation(fig, plot_frame, frames=len(frames), interval=100, repeat=False) 
#can last from a few seconds to several minutes sometimes, nothing abnormal here, 
# except if the simulation is particularly heavy, then it may be better to cut or change the interval

# CHANGE TITLE to AVOID ARASING THE PREVIOUS VIDEO
animation.save("new2D.mp4", writer=writer)