import os
os.environ['PATH']=os.environ['PATH']+':/usr/local/bin'
import matplotlib
import matplotlib.pyplot as pl
import numpy as np
import matplotlib.animation as anim
import matplotlib.axes as ax
from numpy import fft as f
from tqdm import tqdm

## param
lb=0.27
kappa=10**-3
D0=1
alpha=0.1
u0=11
T=[0]
t=0

def D(u):
    return D0*np.exp(-lb*u)*(1-lb*u/2)

def Df(u):
    Du=D0*np.exp(-lb*u)*(1-lb*u/2)
    return f.irfft2(f.rfft2(Du)*filtre2)
def plotsol():
    pl.imshow(v,extent=[-lx,lxa,-ly,ly])
    texte = 'iteration ' + str(k)
    pl.xlabel(texte)
    pl.draw()
    pl.show()
    pl.pause(0.001)
    pl.clf()
    return 0

phi=lb*u0/2
R=D0/np.sqrt(alpha*kappa)
qc=np.sqrt(2*alpha/abs(D(u0)))
lc=2*np.pi/qc
def Rc(phi):
    return 2*np.exp(2*phi)/(phi-1)
##
e=5*10**-2

def RKU(u):
    return div(Df(u)*grad(u))+alpha*u*(1-u/u0)-kappa*nabla4(u)


Lx,Ly=(100,100)
zoom=8
lx,ly=(Lx/zoom,Ly/zoom)
Nx,Ny=(256,256)
p=512*4
#Dx=int(2*L/128)
x,y=np.mgrid[-Lx:Lx:1j*Nx,-Ly:Ly:1j*Ny]

sigma=Lx/200
u=1/(2*np.pi*sigma)*np.exp(-(x**2+y**2)/(2*sigma**2))

u*=1/np.max(u)*1.1*u0

xi,eta=f.fftfreq(Nx)*Nx*2*np.pi/Lx, f.rfftfreq(Ny)*Ny*2*np.pi/Ly
xi, eta=np.meshgrid(xi,eta)

xi=np.transpose(xi)
eta=np.transpose(eta)

K=np.abs(np.max(xi))
filtre1 = np.ones_like(xi)
filtre2 = np.ones_like(xi)

filtre2[np.where(np.abs(xi)>np.max(xi)*1./2)] = 0.
filtre2[np.where(np.abs(eta)>np.max(eta)*1./2)] = 0.
filtre1[np.where(np.sqrt(np.abs(xi)**2+np.abs(eta)**2)>K)] = 0.



def grad(u):
    uhat=f.rfft2(u)*filtre1
    Duhatx,Duhaty=1j*xi*uhat,1j*eta*uhat
    Dux,Duy=f.irfft2(Duhatx),f.irfft2(Duhaty)
    return np.array([Dux,Duy])
def div(V):
    Vhatx,Vhaty=f.rfft2(V)
    Duhat=1j*xi*Vhatx*filtre1+1j*eta*Vhaty*filtre1
    return np.real(f.irfft2(Duhat))
def nabla4(u):
    uhat=f.rfft2(u)*filtre1
    D4uhat=((1j*xi)**2+(1j*eta)**2)**2*uhat
    return f.irfft2(D4uhat)

def plotp(u):
    uh=f.rfft2(u)
    q=int((p-Nx)/2)
    uh=f.fftshift(uh,axes=0)
    uh=np.pad(uh, ((q, q),(0,q)), 'constant',constant_values=((0,0),(0,0)))
    uh=f.ifftshift(uh,axes=0)
    v=f.irfft2(uh)
    v*=np.max(u)/np.max(v)
    o=int(v.shape[0]/2)
    j=int(v.shape[0]/(2*zoom))
    return v[o-j:o+j,o-j:o+j]


frames=[plotp(u)]

##
skip=2000
Dt=0.0001

for k in tqdm(range(10**5)):
    U1=Dt*RKU(u)
    U2=Dt*RKU(u+U1/2)
    U3=Dt*RKU(u+U2/2)
    U4=Dt*RKU(u+U3)
    u+=U1/6+U2/3+U3/3+U4/6
    u=u*(u>0)
    t+=Dt
    if np.isnan(np.sum(u)):
        break
    if int(k/skip)==k/skip:
        v=plotp(u)
        #plotsol()
        frames.append(v.copy())
        T.append(t)

## animation
matplotlib.use('Agg')
Writer = anim.writers['ffmpeg']
writer = Writer(fps=1, metadata=dict(artist='Me'), bitrate=1800)

fig = pl.figure(figsize=(11,5)); axes = fig.add_subplot(111)
line= pl.imshow(frames[0], animated=True, extent=[-lx,lx,-ly,ly])
axes.set_xlabel('x (µm)')
axes.set_ylabel('y (µm)')
fig.suptitle(r'$\alpha=$'+str(alpha)+' '+r'$\lambda =$'+str(lb)+' '+r'$\kappa=$'+str(kappa), fontsize=12)
pl.colorbar()

def plot_frame(i):
    line.set_array(frames[i])
    t=str(T[i])
    if T[i]<0.1:
        axes.set_title('t='+t[0:7]+'s')
    elif T[i]<1:
        axes.set_title('t='+t[0:5]+'s')
    elif T[i]<10:
        axes.set_title('t='+t[0:3]+'s')
    else:
        k=int(np.log10(T[i]))+1
        axes.set_title('t='+t[0:k]+'s')
    fig.canvas.draw()
    return fig

# Animate the solution
animation=anim.FuncAnimation(fig, plot_frame, frames=len(frames), interval=100, repeat=False)
animation.save("256_d.mp4", writer=writer)
