### simulation ###

import numpy as np
import numpy.fft as f #fast Fourier transform module
import time #to print execution time

''' 
This is just a prototype of simulation code for benchmarking or experimentations

Especially, this code do not keep the simulations results, and does not plot anything

It just print the run time
'''

title = "test1"

T=[0]
t=0
L=140
N=1024

# real space poinnts
x=np.linspace(-L/2,L/2,N) #note: for simulation only, it is used only to define in real space the initial solution

#complex space point
xi=f.rfftfreq(N)*N*2*np.pi/L


# dealiasing filter
filtre = np.ones_like(xi)
filtre[np.where(np.abs(xi)>np.max(xi)*2./3)] = 0.


#the gradients, using (real) FFT
def nabla(u):
    uhat=f.rfft(u)
    Duhat=1j*xi*uhat
    return f.irfft(Duhat)

def nabla2(u):
    A=time.time()
    uhat=f.rfft(u)
    Duhat=1j*xi*uhat
    du=f.irfft(Duhat)
    return time.time()-A

def nabla4(u):
    uhat=f.rfft(u)
    D4uhat=(1j*xi)**4*uhat
    return f.irfft(D4uhat)


#params TODO: add arguments supports to execute files like this directly in command lines like for the C code
lb=0.27
kappa=10**-3
D0=1
alpha=0.1
u0=11
phi=lb*u0/2
R=D0/np.sqrt(alpha*kappa)

def D(u):
    return D0*np.exp(-lb*u)*(1-lb*u/2)
qc=np.sqrt(2*alpha/abs(D(u0)))

# initial condition is gaussian
sigma=6 # very often written as proportional to L, or even better 1/qc, to obtain a meaningful value
u=1/np.sqrt(2*np.pi*sigma**2)*np.exp(-x**2/(2*sigma**2)) 

u*=1/np.max(u)*1.1*u0 #to observe the patterns and the expansion it is better to start with an initial density higher than the binodal (and even better the spinodal)
frames=[u.copy()]

# just for clarity, put everything into a function (TODO: compile this with jax or numba)
def RKU(u):
    return nabla(D(u)*nabla(u))+alpha*u*(1-u/u0)-kappa*nabla4(u)

skip=20000
Dt=0.001

A=time.time()

for k in range(5*10**5):
    #RK4 implementation
    U1=Dt*RKU(u)
    U2=Dt*RKU(u+U1/2)
    U3=Dt*RKU(u+U2/2)
    U4=Dt*RKU(u+U3)
    u+=U1/6+U2/3+U3/3+U4/6
    t+=Dt
    u=f.irfft(f.rfft(u)*filtre) #apply dealiasing filter
    u=u*(u>10**-8) #avoid non physics negative values
    if np.isnan(np.sum(u)):
        break
    if int(k/skip)==k/skip:
        print(np.min(u),np.max(u)) #print min and max to monitor progress (optional)
        frames.append(u.copy()) #keep densities and times [useless here]
        T.append(t)

B=time.time()
print(B-A) #execution duration

### save for later ###

### animation ###

import os
os.environ['PATH']=os.environ['PATH']+':/usr/local/bin'
import matplotlib
import matplotlib.pyplot as pl
import matplotlib.animation as anim
import matplotlib.axes as ax

matplotlib.use('Agg')
Writer = anim.writers['ffmpeg']
writer = Writer(fps=4, metadata=dict(artist='Me'), bitrate=1800)

fig = pl.figure(figsize=(11,5)); axes = fig.add_subplot(111)
line, = axes.plot([],[])

axes.set_xlabel('x (µm)')
axes.set_ylabel('u')
axes.set_xlim((-L/2,L/2))
axes.set_ylim(0,1.8)


def plot_frame(i):
    line.set_data(x,frames[i])
    t=str(T[i])
    if T[i]<0.1:
        axes.set_title('t='+t[0:7]+'s')
    elif T[i]<1:
        axes.set_title('t='+t[0:5]+'s')
    elif T[i]<10:
        axes.set_title('t='+t[0:3]+'s')
    else:
        k=int(np.log10(T[i]))+1
        axes.set_title('t='+t[0:k]+'s')
    fig.canvas.draw()
    return fig

# Animate the solution
animation=anim.FuncAnimation(fig, plot_frame, frames=len(T), interval=100, repeat=False)
animation.save(title+".mp4", writer=writer)