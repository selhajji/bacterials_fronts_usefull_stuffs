import matplotlib.pyplot as pl
import numpy as np

''' 
Results from (previous) simulations, investigating the rings to dots transition (in 2D)

Results are of course given in phase space, so (phi,R)
'''


fig = pl.figure(figsize=(11,5))
axes = fig.add_subplot(111)
#labelph=r'$\phi$'
axes.set_xlabel(r'$\phi$')
axes.set_ylabel('R')
axes.set_xlim(0.95,1.7)
axes.set_ylim(0,500)

phi=np.linspace(1.01,1.7,200)

def Rc(phi):
    return 2*np.exp(2*phi)/(phi-1)

label1='onset of the linear instability'
label2='supercritical region left bound'
label3='supercritical region right bound'
label4='Limit of the stability region of the hexagons at leading order'


pl.plot(phi,Rc(phi),label=label1)
pl.axvline(x=1.08,ymin=0,ymax=500,label=label2,linestyle='--',color='r')
pl.axvline(x=1.58,ymin=0,ymax=500,label=label3,linestyle='--',color='r')
pl.axvline(x=1.366,ymin=0,ymax=500,label=label4,linestyle='--',color='b')

edot=np.array([0.05,0.1,0.2,0.4,0.1,0.2,0.2,0.3,0.05,(200-Rc(1.4))/Rc(1.4)])
phidot=np.array([1.1,1.1,1.1,1.1,1.2,1.2,1.3,1.3,1.2,1.4])
Rdot=Rc(phidot)*(1+edot)
labeld0='no instability'
labeld1='unstable rings, dots formation'
labeld2='metastable rings, dots formation'
labelnd='stable rings, no dots'
pl.scatter(phidot,Rdot,label=labeld1,marker='v')

emeta=np.array([0.1,0.4,0.3,0.25,0.5,0.8,0.05])
phimeta=np.array([1.3,1.4,1.4,1.38,1.5,1.55,1.3])
Rmeta=Rc(phimeta)*(1+emeta)
pl.scatter(phimeta,Rmeta,label=labeld2,marker='s')

endot=np.array([0.2,0.1,0.1,0.2,0.4,0.1,0.2,0.4,0.2,0.4])
phindot=np.array([1.37,1.4,1.5,1.5,1.5,1.55,1.55,1.55,1.45,1.6,])
Rndot=Rc(phindot)*(1+endot)
pl.scatter(phindot,Rndot,label=labelnd,marker='o')

eno=np.array([0.1,0.4,0.3,0.25,0.5,0.8])
phino=np.array([0.98,1.2])
Rno=np.array([200,Rc(1.2)*(1-0.1)])
pl.scatter(phino,Rno,label=labeld0,marker='4')

axes.legend()

pl.show()

#%% with e
fig = pl.figure(figsize=(11,5))
axes = fig.add_subplot(111)
#labelph=r'$\phi$'
axes.set_xlabel(r'$\phi$')
axes.set_ylabel('e')
axes.set_xlim(1.0,1.7)
axes.set_ylim(0,0.9)

phi=np.linspace(1.01,1.7,200)

def Rc(phi):
    return 2*np.exp(2*phi)/(phi-1)

label1='onset of the linear instability'
label2='supercritical region left bound'
label3='supercritical region right bound'
label4='Limit of the stability region of the hexagons at leading order'


pl.axvline(x=1.08,ymin=0,ymax=500,label=label2,linestyle='--',color='r')
pl.axvline(x=1.58,ymin=0,ymax=500,label=label3,linestyle='--',color='r')
pl.axvline(x=1.366,ymin=0,ymax=500,label=label4,linestyle='--',color='b')


edot=np.array([0.05,0.1,0.2,0.4,0.1,0.2,0.2,0.3,0.05])
phidot=np.array([1.1,1.1,1.1,1.1,1.2,1.2,1.3,1.3,1.2])
Rdot=Rc(phidot)*(1+edot)
labeld0='no instability'
labeld1='unstable rings, dots formation'
labeld2='metastable rings, dots formation'
labelnd='stable rings, no dots'
pl.scatter(phidot,edot,label=labeld1,marker='v')

emeta=np.array([0.1,0.4,0.3,0.25,0.5,0.8,0.05])
phimeta=np.array([1.3,1.4,1.4,1.38,1.5,1.55,1.3])
Rmeta=Rc(phimeta)*(1+emeta)
pl.scatter(phimeta,emeta,label=labeld2,marker='s')

endot=np.array([0.2,0.1,0.1,0.2,0.4,0.1,0.2,0.4,0.2,0.4,(200-Rc(1.4))/Rc(1.4)])
phindot=np.array([1.37,1.4,1.5,1.5,1.5,1.55,1.55,1.55,1.45,1.6,1.4])
Rndot=Rc(phindot)*(1+endot)
pl.scatter(phindot,endot,label=labelnd,marker='o')

eno=np.array([0.1,0.4,0.3,0.25,0.5,0.8])
phino=np.array([0.98,1.2])
Rno=np.array([200,Rc(1.2)*(1-0.1)])
pl.scatter(phino,eno,label=labeld0,marker='4')

axes.legend()

pl.show()

