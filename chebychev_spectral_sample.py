import numpy as np
from numpy import fft as f
import matplotlib.pyplot as pl

''' 
exemple of Chebychev spectral simulation

It was a possible axis of research of my internship. 
However due to the very promising results achieved in Fourier it was quite abandonned
I came back on those ideas when I had some time

'''

##
T=[0]
t=0

L=75

N=128
xi = np.arange(0,N+1)
x=np.pi*np.arange(0,N+1)/N #uniform grid
y=-np.cos(x) #chebychev's points
#xi=np.arange(0,N)
Y=L*(y+1)/2

lb=0.27
kappa=10**-3 #10**-4

D0=1
alpha=0.1 #0.02
u0=11  #15

filtre = np.ones_like(xi)
filtre[np.where(np.abs(xi)>np.max(xi)*2./3)] = 0.


def plotsol():
    pl.plot(Y,u)
    #pl.axis([-L/2,L/2,0,15])
    pl.axis([0,L,0,14])
    texte = 'iteration ' + str(k)
    pl.xlabel(texte)
    pl.draw()
    pl.show(block=False)
    #print(k)
    pl.pause(0.001)
    pl.clf()
    return 0

sigma=4
u=1/np.sqrt(2*np.pi*sigma**2)*np.exp(-Y**2/(2*sigma**2))

u*=1/np.max(u)*u0*1.1

frames=[u.copy()]

zero=np.zeros_like(u)
dx1=-1/np.sqrt(1-y[1:N-1]**2)
dx2=y[1:N-1]*(1-y[1:N-1]**2)**(-3/2)
dx3=(1-y[1:N-1]**2)**(-3/2)-3*y[1:N-1]**2*(1-y[1:N-1]**2)**(-5/2)
dx4=-9*y[1:N-1]*(1-y[1:N-1]**2)**(-5/2)+15*y[1:N-1]**3*(1-y[1:N-1]**2)**(-7/2)
b=4*dx3*dx1+3*dx1**2





def D(u):
    return D0*np.exp(-lb*u)*(1-lb*u/2)
def D1(u):
    return -lb/2*D0*np.exp(-lb*u)*(3-lb*u)


def RKU(u):
    w1=zero
    w2=w1;w4=w1
    V = np.flipud(u[1:N]); v = list(V)+list(u);
    uh=f.rfft(v)
    uh=uh*(xi<np.max(xi)/2)
    v1=f.irfft(1j*xi*uh)[N:2*N-2]

    v2=f.irfft((1j*xi)**2*uh)[N:2*N-2]
    v3=f.irfft((1j*xi)**3*uh)[N:2*N-2]
    v4=f.irfft((1j*xi)**4*uh)[N:2*N-2]
    w1[1:N-1]=v1*dx1
    w1[0] =0 #sum(xi**2*uh[xi])/N
    w1[N] =0 #sum((-1)**(xi+1)*xi**2*uh[xi])/N
    w2[1:N-1]=v2*dx1**2+v1*dx2
    w2[0] = sum((xi<np.max(xi)/2)*xi**2*(xi**2-1)/3*uh[xi])/(N+1)
    w2[N] = sum((xi<np.max(xi)/2)*(-1)**xi*xi**2*(xi**2-1)/3*uh[xi])/(N+1)
    w4[1:N-1]=v4*dx1**4+6*v3*dx2*dx1**2+v2*b
    w4[0] = sum((xi<np.max(xi)/2)*xi**2*(xi**2-1)*(xi**2-4)*(xi**2-9)/105*uh[xi])/(N+1)
    w4[N] = sum((xi<np.max(xi)/2)*(-1)**xi*xi**2*(xi**2-1)*(xi**2-4)*(xi**2-9)/105*uh[xi])/(N+1)
    w1=w1*1/L
    w2=w2*1/L**2
    w4=w4*1/L**4
    print(np.max(abs(w4)))
    return D(u)*w2+D1(u)*w1**2+alpha*u*(1-u/u0)-kappa*w4


skip=500
Dt=0.00001

#main simulation loop, nearly no changes here
for k in range(6*10**5):
    U1=Dt*RKU(u)
    U2=Dt*RKU(u+U1/2)
    U3=Dt*RKU(u+U2/2)
    U4=Dt*RKU(u+U3)
    u+=U1/6+U2/3+U3/3+U4/6
    t+=Dt
    V = np.flipud(u[1:N]); v = list(V)+list(u) #except 
    v=f.irfft(f.rfft(v)*filtre)
    u[1:N]=v[N:2*N-1]
    u=u*(u>10**-8)
    if np.isnan(np.sum(u)):
        break
    if int(k/skip)==k/skip:
        print(np.min(u),np.max(u),t)
        #frames.append(u.copy())
        #T.append(t)
        #plotsol()

##
w1=zero
w2=w1;w4=w1
M=len(x)-1
ii = np.arange(0,M)
b = list(ii); b.append(0); b = b + list(np.arange(1-M,0));
xi=ii
